package com.zenika.academy.springhelloworld;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class HelloWorldConsolePrinter {

    private static Logger log = LoggerFactory.getLogger(HelloWorldConsolePrinter.class);

    @Autowired
    public HelloWorldConsolePrinter(
            @Value("${hello-world-app.welcome-msg}") String welcomeMessage
    ) {
        log.info("Hello world from constructor");
        log.info(welcomeMessage);
    }

    @PostConstruct
    public void printHelloWorld() {
        log.info("Hello, world!");
    }
}
