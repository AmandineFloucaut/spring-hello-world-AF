package com.zenika.academy.springhelloworld;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class PasswordGenerator implements CommandLineRunner {
    private Random random;
    
    @Autowired
    public PasswordGenerator(Random randomBean) {
        this.random = randomBean;   
    }
    
    @Override
    public void run(String... args) throws Exception {
        String randomPassword = String.valueOf(random.nextLong());

        System.out.println(randomPassword);
    }
}
